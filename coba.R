#coba
library(httr)
library(data.table)
library(plyr)
library(ggplot2)
#Login
url <- "http://192.168.10.151:7979/api/auth/login"
body <- list(email="admin@yahoo.com", password="123qwe")
r <- POST(url,body=body,encode = "json")
b <- httr::content(r, "parsed", "application/json", encoding="UTF-8")
auth <- (paste('Bearer',b[[1]],sep=" "))

#data Mesin
result <- GET("http://192.168.10.151:7979/api/machines",add_headers(Authorization = paste(auth)))
machines <- httr::content(result, "parsed", "application/json", encoding="UTF-8")
mesin <- machines$data
pjgmesin <- length(mesin)
mesin1 <- data.frame(matrix(ncol=3, nrow=pjgmesin))
for(i in seq(from=1, to=pjgmesin, by=1)){
  mesin1[i,1]<- mesin[[i]][["id"]]
  mesin1[i,2]<- mesin[[i]][["code"]] 
  mesin1[i,3]<- mesin[[i]][["name"]]
}
setnames(mesin1, old = c('X1','X2','X3'), new = c('machine_id','code','name'))

#data Reason
urlReason <- "http://192.168.10.151:7979/api/reasons"
reason <- GET(urlReason, encode="json",add_headers(Authorization = paste(auth)))
dataReason <- httr::content(reason, "parsed", "application/json", encoding="UTF-8")
dataReason <- dataReason$data
pjgReason<- length(dataReason)
dataReason1 <- data.frame(matrix(ncol=7, nrow=pjgReason)) 
for(i in seq(from=1, to=pjgReason, by=1)){
  dataReason1[i,1]<- dataReason[[i]][["id"]]
  dataReason1[i,2]<- dataReason[[i]][["reason_category_id"]]
  dataReason1[i,3]<- dataReason[[i]][["name"]]
  dataReason1[i,4]<- dataReason[[i]][["reason_category"]][["id"]]
  dataReason1[i,5]<- dataReason[[i]][["reason_category"]][["name"]]
  dataReason1[i,6]<- dataReason[[i]][["reason_category"]][["code"]]
  dataReason1[i,7]<- dataReason[[i]][["reason_category"]][["color"]]
}
setnames(dataReason1, old = c('X1','X2','X3','X4','X5','X6','X7'), 
         new = c('reason_id','reason_category_id','name','id_reason_cat','name_reason_cat','code','color'))

metadataFunc <- function(start_date,end_date) {
     #data MesinValue
     #2018-11-01&end_date=2018-12-31
     urlMesinValue <- "http://192.168.10.151:7979/api/machineValues?start_date=start_date&end_date=end_date"
     r2 <- GET(urlMesinValue, query = list(start_date=start_date,end_date=end_date), encode="json", verbose(),add_headers(Authorization = paste(auth)))
     machinesValue <- httr::content(r2, "parsed", "application/json", encoding="UTF-8")
     machinesValue <- machinesValue$data
     machinesValueFunc(machinesValue)
}

machinesValueFunc <- function(machinesValue){
  pjgmesinval <- length(machinesValue)
  machinesValue1 <- data.frame(do.call(rbind, machinesValue))
  machinesValue1$created_at <- NULL
  machinesValue1$updated_at <- NULL
  machinesValue1$id <- as.integer(machinesValue1$id)
  machinesValue1$machine_id <- as.integer(machinesValue1$machine_id)
  machinesValue1 <- join(mesin1, machinesValue1, by="machine_id", type="inner")
  #data MesinValue period MachineValue
  periodMesinValue <- machinesValue1$period
  periodMesinValue1 <- data.frame(do.call(rbind, periodMesinValue),cbind(machinesValue1$id),cbind(machinesValue1$machine_id))
  periodMesinValue1$created_at <- NULL
  periodMesinValue1$updated_at <- NULL
  periodMesinValue1$deleted_at <- NULL
  
  #data MesinValue meta_data
  metadataMesinValue <- machinesValue1$meta_data
  for (i in seq_len(length(metadataMesinValue))) {
    assign(paste0("metadata_value", i), data.frame(do.call(cbind, metadataMesinValue[[i]]),i))
  }
 
  library(data.table)
  metadataList <- list()
  for (i in seq_len(length(metadataMesinValue))) {
    meta <- paste("metadata_value", i, sep="")
    data <- mget(paste("metadata_value", i, sep=""))
    metadataList[meta] <- data
  }
  metadataValue <- rbindlist(metadataList, fill = TRUE)
  
  #data MesinValue operator login
  for (i in seq_len(length(metadataMesinValue))) {
    assign(paste0("opLogin", i), data.frame(do.call(rbind, metadataMesinValue[[i]]$operator_login),cbind(i)))
  }
  
  #for (i in seq_len(length(metadataMesinValue))) {
  #  operatorLogin <- do.call(rbind, lapply( paste0("opLogin", 1:length(metadataMesinValue)), get))
  #}
  
  operatorLoginList <- list()
  for (i in seq_len(length(metadataMesinValue))) {
    op <- paste("opLogin", i, sep="")
    login <- mget(paste("opLogin", i, sep=""))
    operatorLoginList[op] <- login
  }
  operatorLogin <- rbindlist(operatorLoginList, fill = TRUE)
  
  
  #data MesinValue plan production
  for (i in seq_len(length(metadataMesinValue))) {
    assign(paste0("planProduct", i), data.frame(do.call(rbind, metadataMesinValue[[i]]$plan_production),cbind(i)))
  }
  library(data.table)
  planProductList <- list()
  for (i in seq_len(length(metadataMesinValue))) {
    plan <- paste("planProduct", i, sep="")
    product <- mget(paste("planProduct", i, sep=""))
    planProductList[plan] <- product
  }
  planProduct <- rbindlist(planProductList, fill = TRUE)
  
  #data MesinValue performance loss
  for (i in seq_len(length(metadataMesinValue))) {
    assign(paste0("performanceLoss", i), data.frame(do.call(rbind, metadataMesinValue[[i]]$performance_loss),cbind(i)))
  }
  #for (i in seq_len(length(metadataMesinValue))) {
   # performanceLoss <- do.call(rbind, lapply( paste0("performanceLoss", 1:length(metadataMesinValue)) , get) )
  #}
  
  performanceLossList <- list()
  for (i in seq_len(length(metadataMesinValue))) {
    performance <- paste("performanceLoss", i, sep="")
    pfloss <- mget(paste("performanceLoss", i, sep=""))
    performanceLossList[performance] <- pfloss
  }
  performanceLoss <- rbindlist(performanceLossList, fill = TRUE)
  setnames(performanceLoss, old = c('reason_id','reason_category_id', 'start_at','finish_at', 'duration'), new = c('pfreason_id','pfreason_category_id', 'pfstart_at','pffinish_at', 'pfduration'))
  
  performanceLoss$pfreason_id <- as.integer(performanceLoss$pfreason_id)
  performanceLoss$pfreason_category_id <- as.integer(performanceLoss$pfreason_category_id)
  performanceLoss$pfstart_at <- as.character(performanceLoss$pfstart_at)
  performanceLoss$pffinish_at <- as.character(performanceLoss$pffinish_at)
  performanceLoss$pfduration <- as.integer(performanceLoss$pfduration)
  
  #data MesinValue availibility loss
  for (i in seq_len(length(metadataMesinValue))) {
    assign(paste0("availibilityLoss", i), data.frame(do.call(rbind, metadataMesinValue[[i]]$performance_loss),cbind(i)))
  }
  #for (i in seq_len(length(metadataMesinValue))) {
   # availibilityLoss <- do.call(rbind, lapply( paste0("availibilityLoss", 1:length(metadataMesinValue)) , get) )
  #}
  availibilityList <- list()
  for (i in seq_len(length(metadataMesinValue))) {
    availibility <- paste("availibilityLoss", i, sep="")
    avloss <- mget(paste("availibilityLoss", i, sep=""))
    availibilityList[availibility] <- avloss
  }
  availibilityLoss <- rbindlist(availibilityList, fill = TRUE)
  setnames(availibilityLoss, old = c('reason_id','reason_category_id', 'start_at','finish_at', 'duration'), new = c('avreason_id','avreason_category_id', 'avstart_at','avfinish_at', 'avduration'))
  availibilityLoss$avreason_id <- as.integer(availibilityLoss$avreason_id)
  availibilityLoss$avreason_category_id <- as.integer(availibilityLoss$avreason_category_id)
  availibilityLoss$avstart_at <- as.character(availibilityLoss$avstart_at)
  availibilityLoss$avfinish_at <- as.character(availibilityLoss$avfinish_at)
  availibilityLoss$avduration <- as.integer(availibilityLoss$avduration)
  
  #join data frame
  metadataValue <- join(metadataValue, operatorLogin, by="i", type="inner")
  metadataValue <- join(metadataValue, planProduct, by="i", type="inner")
  
  #join availibility,performance dan reason
  availibilityLoss <- merge(availibilityLoss, dataReason1, by.x='avreason_id', by.y='reason_id', all.y=TRUE)
  performanceLoss <- merge(performanceLoss, dataReason1, by.x='pfreason_id', by.y='reason_id', all.y=TRUE)
  availibilityLoss <- merge(availibilityLoss, machinesValue1, by.x='i', by.y='machine_id', all.y=TRUE)
  #metadataValue <- merge(availibilityLoss, metadataValue, by.x='i', by.y='i', all.y=TRUE)
  
  return(metadataValue)
  #plot <- ggplot(metadataValue, aes(x = code.y, y = id, fill = oee)) + geom_bar(stat='identity') + geom_text(aes(label=oee),vjust=-0.4,size=2.5)+ theme_minimal()
  #plot + ggtitle("Availibility Loss") + xlab("Nama Mesin") + ylab("Durasi (menit)") + labs(fill = "Jenis Reason") + theme(
    #plot.title = element_text(color="black", size=14, face="bold"),
    #axis.title.x = element_text(color="blue", size=12, face="bold"),
    #axis.title.y = element_text(color="#993333", size=12, face="bold")
  #) 
  #return(plot)
}

plotFunc <- function(metadataValue){
plot <- ggplot(metadataValue, aes(x = code.y, y = id, fill = oee)) + geom_bar(stat='identity') + geom_text(aes(label=oee),vjust=-0.4,size=2.5)+ theme_minimal()
plot + ggtitle("Availibility Loss") + xlab("Nama Mesin") + ylab("Durasi (menit)") + labs(fill = "Jenis Reason") + theme(
  plot.title = element_text(color="black", size=14, face="bold"),
  axis.title.x = element_text(color="blue", size=12, face="bold"),
  axis.title.y = element_text(color="#993333", size=12, face="bold")
) 
return(print(plot))
}

